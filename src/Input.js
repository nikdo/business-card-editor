import React from 'react'

export default ({ name, label, value, setValue, maxLength }) => (
	<div className="pure-control-group">
		<label htmlFor={name}>{label}:</label>
		<input id={name} name={name}
			type="text"
			value={value}
			onChange={(e) => setValue(e.target)}
			maxLength={maxLength} />
	</div>
)