import React from 'react'
import _ from 'underscore'
import { toRgb } from './utils'

export default ({ values, font, styles, minSize, maxSize, showSecondPage, showCompany }) => {
	styles = _.mapObject(styles, (style, key) => {return {
		color: toRgb(style.color),
		fontSize: `${Math.min(maxSize, Math.max(minSize, style.size))}px`,
		fontFamily: style.font
	}})
	return (
		<div className="preview">
			<div className="front">

				{!showCompany ? null :
					<div className="company" style={styles.company}>
						{values.company}
					</div>
				}

				<div className="fullname" style={styles.name}>
					<span className="title_before">{values.title_before} </span>
					<span className="name">{values.name}</span>
					<span className="title_after">{values.title_after ? ', ' + values.title_after : ''}</span>
				</div>

				<div className="title" style={styles.title}>{values.title}</div>

				<div className="address" style={styles.address}>
					{!showCompany ? null : <div className="company_address">{values.company}</div>}
					<div className="city">{values.city}</div>
					<div className="street">{values.street}</div>
				</div>

				<table className="contacts" style={styles.contacts}>
					<tbody>
						{values.phone ? <tr className="phone"><th>Tel.:</th><td>{values.phone}</td></tr> : null}
						{values.email ? <tr className="email"><th>E-mail:</th><td>{values.email}</td></tr> : null}
						{values.web ? <tr className="web"><th>Web:</th><td>{values.web}</td></tr> : null}
					</tbody>
				</table>
			</div>
			<div className={showSecondPage ? 'back' : 'back inactive'}>
				{!showCompany ? null :
					<div className="company" style={styles.company}>
						{values.company}
					</div>
				}
			</div>
		</div>
	)
}