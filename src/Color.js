import React from 'react'
import { toRgb } from './utils'

export default ({ name, availableColors, color, setStyle }) => (
	<select
		name={name + '-color'}
		value={color}
		className="color-selection"
		onChange={(e) => setStyle(name, 'color', e.target.value)}>
			{availableColors.map((c) =>
				<option key={c.code} value={c.code} style={{backgroundColor: toRgb(c.code)}}>
						{c.name}
				</option>
			)}
	</select>
)