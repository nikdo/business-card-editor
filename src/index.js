import 'babel-polyfill'
import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'

const rootElement = document.getElementById('editor')
ReactDOM.render(
	<App />,
	rootElement
)
