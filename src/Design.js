import React from 'react'
import _ from 'underscore'
import wurl from 'wurl'
import Color from './Color'

export default ({ availableColors, availableFonts, styles, setStyle, minSize, maxSize, twoPages, setTwoPages }) => (
	<section>
		<h2>Vzhled</h2>

		{_.map(styles, (style, name) => (
			<div key={name} className="pure-control-group">
				<label>{style.label}:</label>
				<Color name={name}
					color={style.color}
					availableColors={availableColors}
					setStyle={setStyle} />
				<select
					name={name + '-font'}
					className="font-selection"
					value={style.font}
					onChange={(e) => setStyle(name, 'font', e.target.value)}>
						{availableFonts.map((f) => <option key={f.name} value={f.family}>{f.name}</option>)}
				</select>
				<input
					type="number"
					name={name + '-size'}
					min={minSize} max={maxSize}
					className="size-selection"
					value={style.size}
					onChange={(e) => setStyle(name, 'size', e.target.value)} />
			</div>
		))}

		<div className="pure-controls">
			<label htmlFor="two-pages" className="pure-checkbox">
				<input id="two-pages" name="two-pages"
					type="checkbox"
					checked={twoPages}
					onChange={(e) => setTwoPages(e.target.checked)} /> Dvě stránky
			</label>
		</div>

		<input type="hidden" name="background" defaultValue={wurl('?background')} />

	</section>
)
