import React from 'react'
import Input from './Input'

export default ({ values, setValue, showCompany }) => (
	<section>
		<h2>Data</h2>

		{!showCompany ? null :
			<Input name="company" label="Společnost"
				maxLength="30"
				value={values.company} setValue={setValue} />
		}
		<Input name="title_before" label="Titul před jménem"
			maxLength="10"
			value={values.title_before} setValue={setValue} />
		<Input name="name" label="Jméno"
			maxLength="50"
			value={values.name} setValue={setValue} />
		<Input name="title_after" label="Titul za jménem"
			maxLength="10"
			value={values.title_after} setValue={setValue} />
		<Input name="title" label="Funkce"
			maxLength="50"
			value={values.title} setValue={setValue} />

		<fieldset>
			<legend>Adresa</legend>
			<Input name="city" label="Město"
				maxLength="50"
				value={values.city} setValue={setValue} />
			<Input name="street" label="Ulice"
				maxLength="80"
				value={values.street} setValue={setValue} />
		</fieldset>

		<fieldset>
			<legend>Kontakty</legend>
			<Input name="phone" label="Telefon"
				maxLength="20"
				value={values.phone} setValue={setValue} />
			<Input name="email" label="E-mail"
				maxLength="40"
				value={values.email} setValue={setValue} />
			<Input name="web" label="Web"
				maxLength="40"
				value={values.web} setValue={setValue} />
		</fieldset>

	</section>
)