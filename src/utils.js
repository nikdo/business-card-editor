export function toRgb(str) {
	return `rgb(${str.replace(/ /g, ',')})`
}