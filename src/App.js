import React, { Component } from 'react'
import _ from 'underscore'
import Preview from './Preview'
import Data from './Data'
import Design from './Design'

const config = window.business_card_config

export default class App extends Component {

	constructor(props) {
		super(props)
		this.state = config.initialState
		if (!config.showCompany)
			this.state.styles = _.filter(this.state.styles, (style, key) => key != 'company')

		this.setValue = this.setValue.bind(this)
		this.setStyle = this.setStyle.bind(this)
	}

	setValue(input) {
		this.setState({
			values: Object.assign({}, this.state.values, {[input.name]: input.value})
		})
	}

	setStyle(name, styleName, value) {
		const style = Object.assign({}, this.state.styles[name], { [styleName]: value })
		this.setState({
			styles: Object.assign({}, this.state.styles, { [name]: style })
		})
	}

	render() {
		return <div className="pure-form pure-form-aligned">
			<Preview
				values={this.state.values}
				styles={this.state.styles}
				minSize={config.minSize}
				maxSize={config.maxSize}
				showSecondPage={this.state.showSecondPage}
				showCompany={config.showCompany}
				/>
			<form action={config.postUrl} method="post">
				<Data
					values={this.state.values}
					setValue={this.setValue}
					showCompany={config.showCompany} />
				<Design
					availableColors={config.colors}
					availableFonts={config.fonts}
					styles={this.state.styles}
					setStyle={this.setStyle}
					minSize={config.minSize}
					maxSize={config.maxSize}
					twoPages={this.state.showSecondPage}
					setTwoPages={(showSecondPage) => this.setState({showSecondPage: showSecondPage})}
					/>
				<section className="submit">
					<input type="submit" value="Uložit" />
				</section>
			</form>
		</div>
	}
}
