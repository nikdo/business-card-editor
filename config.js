window.business_card_config = {

	fonts: [
		{family: 'bahamal0', name: 'Bahamas Light' },
		{family: 'bahamas0', name: 'Bahamas' },
		{family: 'bahamasb', name: 'Bahamas Bold' },
		{family: 'desyrel', name: 'Desyrel' },
		{family: 'koalab', name: 'Koala Bold' },
		{family: 'oldstandard-bold', name: 'Old Standard TT Bold' },
		{family: 'oldstandard-regular', name: 'Old Standard TT Regular' },
		{family: 'xaltrn2u', name: 'XalTerion' }
	],

	colors: [
		{code: '255 0 0', name: 'Červená'},
		{code: '0 0 255', name: 'Modrá'},
		{code: '0 0 0', name: 'Černá'}
	],

	initialState: {
		values: {
			title_before: 'Ing.',
			title_after: 'MBA',
			name: 'Karel Womáčka',
			company: 'Moje Firma',
			title: 'generální ředitel',
			city: '679 61 Kotěhůlky',
			street: 'Dolní Kotěhůlky 42',
			phone: '+420 123 456 789',
			email: 'womacka@mojefirma.cz',
			web: 'mojefirma.com'
		},
		font: 'bahamal0',
		styles: {
			company: {label: 'Společnost', color: '255 0 0', font: 'bahamasb', size: 18},
			name: {label: 'Jméno', color: '0 0 0', font: 'bahamasb', size: 15},
			title: {label: 'Funkce', color: '0 0 0', font: 'bahamal0', size: 10},
			address: {label: 'Adresa', color: '0 0 0', font: 'bahamal0', size: 10},
			contacts: {label: 'Kontakty', color: '0 0 0', font: 'bahamal0', size: 10}
		},
		showSecondPage: false
	},

	minSize: 6,
	maxSize: 18,
	postUrl: 'http://example.com',
	showCompany: true
}
