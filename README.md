# Business Card Editor

## Development

Start watching for changes in JavaScript source files and compile on change:
	
	`npm start`

Basic configuration can be found in `src/config.js`.

## Deployment

1. Build compressed script for production:

	`npm run build`

2. Copy following files to webhosting:
	- `fonts/`
	- `background-front.png`
	- `background-back.png`
	- `config.js`
	- `editor.js`
	- `fonts.css`
	- `index.html`
	- `styles.css`

## Project Structure

- `fonts/` - Fonts for business card. When adding font don't forget to modify `config.js` and `fonts.css`.
- `src/` - JavaScript source files written with [React](https://facebook.github.io/react/) and [Babel](http://babeljs.io) for ES2015 syntax. After modification the code has to be compiled.
- `background-front.png` - Business card front page background.
- `background-back.png` - Business card back page background.
- `config.js` - Configuration for editor. Modification does not require recompilation.
- `editor.js` - Compiled application to be included in HTML.
- `fonts.css` - `@font-face` definitions for business card fonts.
- `index.html` - Business card editor is initialized on element with id `editor`.
- `package.json` - [NPM](https://www.npmjs.com) configuration file.
- `styles.css` - Basic page styling and business card preview styles. Use this to **modify business card layout**.

## Configuration File

Following sections can be found in `config.js` configuration file:

- `fonts` - Available fonts where `family` is the font family name defined in `fonts.css` and `name` is the name displayed to the user.
- `colors` - Available font colors where `code` is color value in `R G B` format and `name` is the name displayed to the user.
- `initialState` - Initial form values. Be careful to choose existing font and colors. Size should fit inside avaliable size range.
- `minSize` - Minimal font size.
- `maxSize` - Maximal font size.
- `postUrl` - URL where the form values are submitted.
- `showCompany` - Whether to show company name in form and preview.
